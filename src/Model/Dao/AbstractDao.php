<?php

namespace App\Model\Dao;

abstract class AbstractDao {

	protected $table;
	protected $classname;

	public function __construct($table, $classname) {
		if(empty($table) || empty($classname)) {
			throw new Exception('You have to define table and classname class attributes');
		}

		$this->table = $table;
		$this->classname = $classname;

	}

	public function list() {
		// Valeur de retour
		$ret = [];

		$pdo = \App\Db::getPDO(); 
		$sth = $pdo->query('SELECT * FROM ' . $this->table);
		$ret = $sth->fetchAll(\PDO::FETCH_CLASS, $this->classname);

		return $ret;

	}
}