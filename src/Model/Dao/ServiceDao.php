<?php

namespace App\Model\Dao;

use App\Model\Service;

class ServiceDao extends AbstractDao {
	public function __construct() {
		parent::__construct('services', 'App\Model\Service');
	}

	public function findById($id) {

		$pdo = \App\Db::getPDO(); 
		$sth = $pdo->prepare('SELECT * FROM ' . $this->table . ' WHERE id = ?');
		$sth->execute([$id]);
		$sth->setFetchMode(\PDO::FETCH_CLASS, $this->classname);
		$ret = $sth->fetch(\PDO::FETCH_CLASS);

		return $ret;
	}

	public function create(Service $service) : Service {

		$sql = 'SET @uuid=UUID();INSERT INTO services (id, title, `image`, `description`) VALUES (@uuid, ?, ?, ?);';
		
		$pdo = \App\Db::getPDO();
		$pdo->beginTransaction();

		$sth = $pdo->prepare($sql);
		$res = $sth->execute([$service->title, $service->image, $service->description]);
		$sth->closeCursor();
		
		if($res === false) {
			throw new \Exception('Could not save the service in the database, reason :' . $pdo->errorCode());
		}

		$res = $pdo->query('SELECT @uuid;');

		if($res === false) {
			throw new \Exception('Could not save the service in the database, reason :' . implode(';',$pdo->errorInfo()));
		}

		$service->id = $res->fetchColumn();
		$pdo->commit();

		return $service;

	}

	public function save(Service $service): Service {

		$sql = 'UPDATE services SET title=?, `image`=?, `description`= ? WHERE id = ?';

		$pdo = \App\Db::getPDO();

		$sth = $pdo->prepare($sql);
		$res = $sth->execute([$service->title, $service->image, $service->description, $service->id]);
		$sth->closeCursor();
		
		if($res === false) {
			throw new \Exception('Could not save the service in the database, reason :' . $pdo->errorCode());
		}

		return $service;
	} 

	public function delete(Service $service): bool {

		$pdo = \App\Db::getPDO();

		$sql = 'DELETE FROM services WHERE id = ?';

		$sth = $pdo->prepare($sql);
		$res = $sth->execute([$service->id]);
		$sth->closeCursor();
		
		if($res === false) {
			throw new \Exception('Could not save the service in the database, reason :' . $pdo->errorCode());
		}

		return true;
	}
}