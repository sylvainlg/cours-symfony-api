<?php

namespace App\Model;

class Service {
	public $id;
	public $title;
	public $image;
	public $description;
}