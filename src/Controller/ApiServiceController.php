<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use App\Model\Dao\ServiceDao;
use App\Model\Service;

/**
 * @Route("/api/service")
 */
class ApiServiceController extends Controller {

	/**
	 * @Route("/", methods={"GET", "POST"}, name="api_service")
	 */
	public function defaultRoute(Request $request) {
		$method = $request->server->get('REQUEST_METHOD');

		$ret = new Response('Method not allowed', Response::HTTP_METHOD_NOT_ALLOWED);

		if($method == "GET") {
			$ret = $this->doGet();
		} else if($method == "POST") {
			$ret = $this->doPost($request);
		}

		return $ret;
	}

	/**
	 * @Route("/{id}", methods={"GET", "PUT", "DELETE"}, name="api_service_with_id")
	 */
	public function withIdRoute($id, Request $request) {
		$method = $request->server->get('REQUEST_METHOD');

		$ret = new Response('Method not allowed', Response::HTTP_METHOD_NOT_ALLOWED);

		if($method == "GET") {
			$ret = $this->doGet($id);
		} else if($method == "PUT") {
			$ret = $this->doPut($id, $request);
		} else if($method == "DELETE") {
			$ret = $this->doDelete($id);
		}

		return $ret;
	}

	/*
	 * @Route("/{id}", methods={"GET"}, name="api_service_get")
	 * 
	 * Paramètre id optionnel car affecté par défaut à null ci-dessous
	 */
	public function doGet($id = null) {

		$serviceDao = new ServiceDao();

		if($id === null) {
			return $this->json($serviceDao->list());
		} else {
			
			$service = $serviceDao->findById($id);
			if(!$service) {
				throw $this->createNotFoundException('The service does not exist');
			}
			return $this->json($service);

		}

	}

	/*
	 * @Route("/", methods={"POST"}, name="api_service_post")
	 */
	public function doPost(Request $request) {

		$service = new Service();
		$serviceDao = new ServiceDao();

		$contentType = $request->headers->get('Content-Type');

		if($contentType === null || $contentType !== 'application/json') {
			return new Response(
				'The request header Content-Type must be set to application/json',
				Response::HTTP_BAD_REQUEST);
		}

		$content = json_decode($request->getContent());

		if($content === null) {
			return new Response(
				'The request must contain a valid json object',
				Response::HTTP_BAD_REQUEST);
		}

		$service->title = $content->title;
		$service->image = $content->image;
		$service->description = $content->description;

		try {
			$serviceDao->create($service);
		} catch(\Exception $e) {
			return new Response(
				'Unexpected error occur : ' . $e->getMessage(),
				500
			);
		}

		return $this->json($service, 201, ['Location' => '/api/service/get/' . $service->id]);

	}

	/*
	 * @Route("/{id}", methods={"PUT"}, name="api_service_put")
	 */
	public function doPut($id, Request $request) {

		$serviceDao = new ServiceDao();
		$service = $serviceDao->findById($id);
		if(!$service) {
			throw $this->createNotFoundException('The service does not exist');
		}

		$content = json_decode($request->getContent());

		if($content === null) {
			return new Response(
				'The request must contain a valid json object',
				Response::HTTP_BAD_REQUEST);
		}

		$service->title = $content->title;
		$service->image = $content->image;
		$service->description = $content->description;

		try {
			$serviceDao->save($service);
		} catch(\Exception $e) {
			return new Response(
				'Unexpected error occur : ' . $e->getMessage(),
				500
			);
		}

		return $this->json($service, Response::HTTP_OK, ['Location' => '/api/service/get/' . $service->id]);

	}

	/*
	 * @Route("/{id}", methods={"DELETE"}, name="api_service_delete")
	 */
	public function doDelete($id) {

		$serviceDao = new ServiceDao();
		$service = $serviceDao->findById($id);
		if(!$service) {
			throw $this->createNotFoundException('The service does not exist');
		}

		$serviceDao->delete($service);

		return new Response(
			null,
			204
		);

	}

}