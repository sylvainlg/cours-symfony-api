<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/api/product")
 */
class ApiProductController extends Controller
{
    /**
     * @Route("/get", name="api_product_get", methods={"GET"})
     */
    public function getAction()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/ProductApiController.php',
        ]);
    }
}
