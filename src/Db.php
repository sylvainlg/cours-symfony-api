<?php

namespace App;

class Db {

	private static $pdo = null;

	public static function getPDO() {

		if(self::$pdo === null) {
			try {
				self::$pdo = new \PDO('mysql:dbname=cesiphp;host=127.0.0.1', 'root', '');
			} catch (\PDOException $e) {
				die('Connexion échouée : ' . $e->getMessage());
			}
		}

		return self::$pdo;
	}
}
